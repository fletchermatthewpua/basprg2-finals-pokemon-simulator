#pragma once
#include <string>
#include <iostream>
#include <conio.h>
#include "Pokemon.h"
#include "Location.h"
#include <vector>

using namespace std;

class Trainer
{
public:
	Trainer();
	string trainerName;
	vector<Pokemon*> myPokemons;
	void addToMyPokemons(Pokemon*pokemonToAdd, vector<Pokemon*> pokemonSlot);
	void trainerMove(Location* trainerLocation, int xNew, int yNew);
};