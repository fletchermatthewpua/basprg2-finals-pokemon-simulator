#pragma once
#include <string>
#include <iostream>
#include <conio.h>
#include <vector>
#include "Pokedex.h"


using namespace std;

class Pokemon
{
public:
	Pokemon();
	//Pokedex* pokedex = new Pokedex();
	Pokemon(string name, int baseHp, int level, int baseDamage, int exp, int expToNextLevel);
	string name;
	int baseHp;
	int hp;
	int level;
	int baseDamage;
	int exp;
	int expToNextLevel;
	void pokemonAttacks(Pokemon* pokemon);
};