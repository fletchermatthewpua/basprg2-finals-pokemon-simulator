﻿#include <iostream>
#include <string>
#include <conio.h>
#include "Trainer.h"
#include "Pokemon.h"
#include "Pokedex.h"
#include "GameManager.h"
#include <vector>

using namespace std;

int main()
{
	Trainer* trainer = new Trainer();
	
	GameManager* gameManager = new GameManager();

	gameManager->gameStart(trainer);

	return 0;
}