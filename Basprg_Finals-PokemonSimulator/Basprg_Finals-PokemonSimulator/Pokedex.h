﻿#pragma once
#include <string>
#include <iostream>
#include <conio.h>
#include "Pokemon.h"
#include <vector>


using namespace std;

class Pokedex
{
public:
	Pokedex();
	Pokemon* pokemon = new Pokemon();
	vector<Pokemon*>pokemonList = { Bulbasaur, Charmander, Squirtle, Caterpie, Weedle, Pidgey, Ratata, Spearow, Ekans, Pikachu, Sandshrew, nidoranGirl, nidoranBoy, Clefairy, Vulpix };
	Pokemon* Bulbasaur = new Pokemon("Bulbasaur", 30, 5, 14, 0, 64); //string name, int baseHp, int level, int baseDamage, int exp, int expToNext;
	Pokemon* Charmander = new Pokemon("Charmander", 24, 1, 17, 0, 62);
	Pokemon* Squirtle = new Pokemon("Squirtle", 29, 1, 15, 0, 63);
	Pokemon* Caterpie = new Pokemon("Caterpie", 25, 1, 10, 0, 39);
	Pokemon* Weedle = new Pokemon("Weedle", 20, 1, 13, 0, 39);
	Pokemon* Pidgey = new Pokemon("Pidgey", 22, 1, 13, 0, 50);
	Pokemon* Ratata = new Pokemon("Ratata", 20, 1, 15, 0, 51);
	Pokemon* Spearow = new Pokemon("Spearow", 20, 1, 16, 0, 39);
	Pokemon* Ekans = new Pokemon("Ekans", 20, 1, 17, 0, 58);
	Pokemon* Pikachu = new Pokemon("Pikachu", 25, 1, 15, 0, 112);
	Pokemon* Sandshrew = new Pokemon("Sandshrew", 22, 1, 17, 0, 60);
	Pokemon* nidoranGirl = new Pokemon("Nidoran(Girl)", 23, 1, 14, 0, 55);
	Pokemon* nidoranBoy = new Pokemon("Nidoran(Boy)", 20, 1, 16, 0, 55);
	Pokemon* Clefairy = new Pokemon("Clefairy", 45, 1, 12, 0, 113);
	Pokemon* Vulpix = new Pokemon("Vulpix", 23, 1, 15, 0, 60);
};