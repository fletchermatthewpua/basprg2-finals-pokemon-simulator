#include "Pokemon.h"
#include <string>
#include <iostream>
#include<conio.h>

using namespace std;

Pokemon::Pokemon()
{
	this->name = "";
	this->level = 0;
	this->baseHp = 0;
	this->hp = 0;
	this->baseDamage = 0;
	this->exp = 0;
	this->expToNextLevel = 0;
}

Pokemon::Pokemon(string name, int baseHp, int level, int baseDamage, int exp, int expToNextLevel)
{
	this->name = name;
	this->level = level;
	this->baseHp = baseHp;
	this->hp = baseHp;
	this->baseDamage = baseDamage;
	this->exp = exp;
	this->expToNextLevel = expToNextLevel;
}

void Pokemon::pokemonAttacks(Pokemon * pokemon)
{
	cout << this->name << " attacked " << pokemon->name << endl;
	pokemon->hp -= this->baseDamage;
	cout << pokemon->name << " remaining hp is " << pokemon->hp << endl;
	system("pause");
}
