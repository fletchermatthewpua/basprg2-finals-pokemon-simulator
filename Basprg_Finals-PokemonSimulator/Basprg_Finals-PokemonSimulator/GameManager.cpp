#include "GameManager.h"
#include <string>
#include <iostream>
#include <conio.h>
#include "Trainer.h"
#include "Pokemon.h"
#include "Pokedex.h"

using namespace std;


GameManager::GameManager()
{
}

void GameManager::gameStart(Trainer* trainer)
{
	int x = 0;
	char y = 'a';
	Trainer* pokemonTrainer = new Trainer();
	Pokemon* pokemon = new Pokemon();
	Pokedex* pokedex = new Pokedex();
	Location* trainerLocation = new Location(0,0);

	cout << R"(             
 __________       __                                    _________.__              .__          __                   
 \______   \____ |  | __ ____   _____   ____   ____    /   _____/|__| _____  __ __|  | _____ _/  |_  ___________    
  |     ___/  _ \|  |/ // __ \ /     \ /  _ \ /    \   \_____  \ |  |/     \|  |  \  | \__  \\   __\/  _ \_  __ \   
  |    |  (  <_> )    <\  ___/|  Y Y  (  <_> )   |  \  /        \|  |  Y Y  \  |  /  |__/ __ \|  | (  <_> )  | \/   
  |____|   \____/|__|_ \\___  >__|_|  /\____/|___|  / /_______  /|__|__|_|  /____/|____(____  /__|  \____/|__|      
                      \/    \/      \/            \/          \/          \/                \/                       
                                                       
	by Fletcher Matthew L. Pua                               
                                                                                                                        
		)" << endl;
	system("pause");
	system("cls");

	cout << "Hello, there trainer!" << endl << "I'm glad to finally meet you!\n\n\n";

	system("pause");
	system("cls");

	cout << "Welcome to the world of POKEMON!" << endl << "My name is Oak, you may call me Professor Oak. I study POKEMON as a profession.\n\n\n";

	system("pause");
	system("cls");

	cout << "Now, tell me a bit about yourself." << endl << "What is your name?\n\n\n";
	cin >> trainer->trainerName;

	system("cls");

	cout << "Alright...\n" << "So your name is " << trainer->trainerName << " \n\n\n";

	system("pause");
	system("cls");

	cout << "There are three different Pokemons placed here.\n" << "Choose one, so you can strat your journey!\n\n\n";

	system("pause");
	system("cls");

	cout << endl << "Which one do you choose?\n"
		<< "[1] Bulbasaur		[Grass Type]		Level 5\n "
		<< "[2] Charmander		 [Fire Type]	    Level 5\n "
		<< "[3] Squirtle		 [Water Type]		Level 5\n " << endl;
	cin >> x; cin.ignore();

	switch (x)
	{
	case 1:
		cout << "You got a Bulbasaur!\n\n\n";
		//trainer->addToMyPokemons(pokedex->Bulbasaur,trainer->myPokemons);
		system("pause");
		system("cls");
		break;
	case 2:
		cout << "You got a Charmander!\n\n\n";
		system("pause");
		system("cls");
		break;
	case 3:
		cout << "You got a Squirtle!\n\n\n";
		system("pause");
		system("cls");
		break;
	}

	cout << "Your journey has begun!\n\n\n";
	system("pause");
	system("cls");

	while (1)
	{
		cout << "What would you like to do?\n"
			<< "[1] - Move " << endl
			<< "[2] - Pokemons " << endl
			<< "[3] - Pokemon Center\n\n";
		cin >> x; cin.ignore();
		switch (x)
		{
		case 1:
			system("cls");
			cout << "Where do you want to move?\n" << "[W] - Up\n [A] - Left\n [D] - Right\n [S] - Down\n";
			cin >> y; cin.ignore();
			switch (y)
			{
			case 'W':
				pokemonTrainer->trainerMove(trainerLocation, 0, 1);
				cout << endl << endl << endl;
				system("pause");
			case 'A':
				pokemonTrainer->trainerMove(trainerLocation, 0, -1);
				cout << endl << endl << endl;
				system("pause");
			case 'D':
				pokemonTrainer->trainerMove(trainerLocation, 1, 0);
				cout << endl << endl << endl;
				system("pause");
			case 'S':
				pokemonTrainer->trainerMove(trainerLocation, -1, 0);
				cout << endl << endl << endl;
				system("pause");
			}
		case 2:
			;
		case 3:
			;
		}
		system("cls");
	}

}
